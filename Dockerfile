from node:latest

workdir /usr/src/app

copy package*.json ./

run npm i

copy . .

expose 3000

cmd ["npm","start"]